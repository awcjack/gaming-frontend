import React from 'react';
import ReactDOM from 'react-dom';
import show from './show.png';
import logo from './gaming.svg';
import account from './account.svg';
import fire from './fire.svg';
import firework from './firework.svg';
import sha512 from 'crypto-js/sha512';
import axios from 'axios';
import './index.css';
import http from 'http';
import * as serviceWorker from './serviceWorker';
var backendServerURL = "https://awcjack.ddns.net/minesweeper/api/users"
class Login extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      userid: '',
      password: '',
      status: '',
    };
  }
  doLogin() {
    var userid = this.state.userid;
    var password = this.state.password;
    var hashedPassword = sha512(password);
    var payload = {
      crossDomain: true,
      "userid": userid,
      "hashedPassword": hashedPassword
    }
    let self = this;
    axios.post(backendServerURL + "/login", payload)
    .then(function(response) {
      if(response.status === 200){
        if(response.data.status === "success"){
          //login success
          self.setState({status: response.data.message});
          self.props.onLogin(userid);
          localStorage.setItem('sessionid', response.data.sessionid);
          setTimeout(()=>{self.props.onClick("minesweeper")},1000); 
        } else if(response.data.status === "error"){
          //password wrong/account not exist
          self.setState({status: response.data.message});
        }
      }
    });
  }
  render() {
    return(
      <div className="mainContent flexBox">
        <h1> Login </h1>
        <img src={logo} alt="logo" className="logo"></img>
        <div>UserID: <input type="text" onChange={(event)=>this.setState({userid: event.target.value})}></input></div>
        <div>Password: <input type="password" onChange={(event)=> this.setState({password: event.target.value})}></input></div>
        {this.state.status}
        <button onClick={()=>this.doLogin()}>Login</button>
        <p>Don't have an account? <span className="fakeLink" onClick={()=>this.props.onClick("register")}> Register Here</span></p>
      </div>
    )
  }
}

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userid: '',
      email: '',
      password: '',
      password2: '',
      status: ''
    }
  }
  doRegister() {
    var userid = this.state.userid;
    var email = this.state.email;
    var password = this.state.password;
    var password2 = this.state.password2;
    if(password != password2){
      this.setState({status: 'Password different, please re-enter.'})
      return;
    }
    var hashedpassword = sha512(password);
    let self = this;
    var payload = {
      crossDomain: true,
      "userid": userid,
      "email": email,
      "hashedpassword": hashedpassword
    }
    axios.post(backendServerURL + "/register", payload)
    .then(function(response){
      if(response.status === 200){
        if(response.data.status === "success"){
          //login success
          self.setState({status: response.data.message});
          setTimeout(()=>{self.props.onClick("login")}, 3000); 
        } else if(response.data.status === "error"){
          //password wrong/account not exist
          self.setState({status: response.data.message});
        }
      }
    });
  }
  render() {
    return(
      <div className="mainContent flexBox">
        <h1> Register </h1>
        <img src={account} alt="account" className="logo"></img>
        <div>Email: <input type="email" onChange={(event)=>this.setState({email: event.target.value})}></input></div>
        <div>UserID: <input type="text" onChange={(event)=>this.setState({userid: event.target.value})}></input></div>
        <div>Password: <input type="password" onChange={(event)=>this.setState({password: event.target.value, status: ''})}></input></div>
        <div>Verify Password: <input type="password" onChange={(event)=>this.setState({password2: event.target.value, status: ''})}></input></div>
        {this.state.status}
        <button onClick={()=>this.doRegister()}>Register</button>
        <p>Already have account? <span className="fakeLink" onClick={()=>this.props.onClick("login")}> Click here.</span></p>
      </div>
    );
  }
}

class Profile extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      first: true,
      error: '',
      username: '',
      email: '',
      minesweeper_easy_time: '',
      minesweeper_medium_time: '',
      minesweeper_hard_time: '',
      minesweeper_custom_size: '',
      minesweeper_custom_time: '',
    }
  }
  doPull(){
    let sessionid = localStorage.getItem('sessionid');
    var payload = {
      sessionid: sessionid,
    };
    var self = this;
    axios.post(backendServerURL+"/profile", payload)
    .then(function(response){
      if(response.status === 200){
        if(response.data.status === "error"){
          self.setState({error: response.data.message, first: false});
        } else if(response.data.status === "success"){
          self.setState({
            username: response.data.username,
            email: response.data.email,
            minesweeper_easy_time: response.data.minesweeper.easy_time,
            minesweeper_medium_time: response.data.minesweeper.medium_time,
            minesweeper_hard_time: response.data.minesweeper.hard_time,
            minesweeper_custom_size: response.data.minesweeper.custom_size,
            minesweeper_custom_time: response.data.minesweeper.custom_time,
            first: false
          });
        }
      }
    });
  }
  render() {
    if(this.state.first){
      this.doPull();
    }
    return(
      <div className="mainContent flexBox">
        {this.state.error}
        <table>
          <tr>
            <th>Username</th>
            <th>{this.state.username}</th>
          </tr>
          <tr>
            <th>Email</th>
            <th>{this.state.email}</th>
          </tr>
          <tr>
            <th  colSpan="2"></th>
          </tr>
          <tr>
            <th colSpan="2">Minesweeper</th>
          </tr>
          <tr>
            <th>Easy (s)</th>
            <th>{this.state.minesweeper_easy_time}</th>
          </tr>
          <tr>
            <th>Medium (s)</th>
            <th>{this.state.minesweeper_medium_time}</th>
          </tr>
          <tr>
            <th>Hard (s)</th>
            <th>{this.state.minesweeper_hard_time}</th>
          </tr>
          <tr>
            <th>Custom Size</th>
            <th>{this.state.minesweeper_custom_size}</th>
          </tr>
          <tr>
            <th>Custom Time (s)</th>
            <th>{this.state.minesweeper_custom_time}</th>
          </tr>
        </table>
      </div>
    );
  }
}

class Minesweeper extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      difficulty: 'easy',
      bombCount: 0,
      time: 0,
      status: '',
      minesweeper: [],
      old_minesweeper: [],
      load: false,
      height: 9,
      width: 9,
      boom: false,
      showBoomModal: false,
      first: true,
      win: false,
      x: '',
      y: '',
      change: false,
      custom: false,
      customUpdate: false,
      oldCustomSize: "0x0",
      oldCustomTime: 0,
    };
  }
  tick() {
    this.setState( (prevState, props) => { 
      return {time: prevState.time+100};
    });
  }
  handleClick(e){
    var id = e.target.id;
    var click = e.type == 'click' ? "left" : "right";
    var i = id.split(" ")[0];
    var j = id.split(" ")[1];
    let sessionid = localStorage.getItem('sessionid');
    var payload = {
      sessionid: sessionid,
      i: i,
      j: j,
      time: this.state.time,
      click: click,
    }
    var self = this;
    axios.post(backendServerURL + '/click', payload)
    .then(function(response){
      if(response.status == 200){
        if(response.data.status == "error"){
          self.setState({status: response.data.message});
        } else if(response.data.status == "success"){
          if(response.data.message == "BOOM!"){
            //pop up
            self.setState({boom: true, minesweeper: response.data.result, x: i, y: j, bombCount: response.data.bombNumber});
          } else if(response.data.message == "Opened" || response.data.message == "Flagged" || response.data.message == "unFlagged"){
            //update button map
            self.setState({minesweeper: response.data.result, x: i, y: j, bombCount: response.data.bombNumber});
          } else if(response.data.message == "win"){
            if(response.data.type == "normal"){
              self.setState({minesweeper: response.data.result, win: true, x: i, y: j, bombCount: response.data.bombNumber});
            } else if(response.data.type == "custom"){
              self.setState({minesweeper: response.data.result, win: true, x: i, y: j, bombCount: response.data.bombNumber, customUpdate: true, oldCustomSize: response.data.custom_size, oldCustomTime: response.data.custom_time});
            }
          }
        }
      }
    });
  }
  Blockvalue(map, i, j){
    if(map.length != 0){
      if(map[i][j] == "F"){
        return (
          //<img className="flag" src={flag}></img>
          "🚩"
        )
      } else if(map[i][j] == "*"){
        return (
          "💣"
        )
      }
        return map[i][j];
    }
  }
  BlockClass(map, i, j){
    if(map.length != 0){
      if(i == this.state.x && j == this.state.y){
        return "mineBlock Opened LastOpen";
      }
      if(map[i][j] != 0){
        return "mineBlock Opened";
      } else {
        return "mineBlock";
      }
    }
  }
  BlockDisabled(map,i,j){
    if(map.length != 0){
      if(map[i][j] != 0){
        if(map[i][j] != "F"){
          return true;
        }
      }
    }
    return false;
  }
  createBlock(id) {
    var i = id.split(" ")[0];
    var j = id.split(" ")[1];
    return (
      <button className={this.BlockClass(this.state.minesweeper,i,j)} id={id} onClick={e=>this.handleClick(e)} onContextMenu={e=>this.handleClick(e)} disabled={this.BlockDisabled(this.state.minesweeper,i,j)}>{(this.Blockvalue(this.state.minesweeper, i, j) == 0 || this.Blockvalue(this.state.minesweeper, i, j) == "_") ? " " : this.Blockvalue(this.state.minesweeper, i, j)}</button>
    )
  }
  removeOldRecord() {
    console.log("old");
    var sessionid = localStorage.getItem('sessionid');
    axios.post(backendServerURL + '/create', {sessionid: sessionid, delete: true, old: true})
    .then();
    var payload = {
      sessionid: sessionid,
      difficulty: this.state.difficulty,
      height: this.state.height,
      width: this.state.width,
    };
    axios.post(backendServerURL + '/create', payload)
    this.setState({load: false, time: 0});
  }
  renderBoomModal() {
    return (
      <div className="modal">
        <div className="modalContent">
          <h2 style={{color: 'red'}}>BOOM!</h2>
          <img src={fire} style={{height: '20vh'}}/><br></br>
          <button onClick={()=>window.location.reload(false)}>Restart</button>
        </div>
      </div>
    )
  }
  renderGameFrame() {
    var height = 0;
    var width = 0;
    if(this.state.difficulty == "easy"){
      height = 9;
      width = 9;
      if(this.state.width != 9){
        this.setState({width: 9});
      }
    } else if(this.state.difficulty == "medium"){
      height = 16;
      width = 16;
      if(this.state.width != 16){
        this.setState({width: 16});
      }
    } else if(this.state.difficulty == "hard"){
      height = 16;
      width = 30;
      if(this.state.width != 30){
        this.setState({width: 30});
      }
    } else {
      //custom
      height = this.state.height;
      width = this.state.width;
    }
    var BlockList = [];
    if(this.state.minesweeper.length == 0 || this.state.change == true){
      var map = [];
      for(var i = 0; i < height; i++){
        var innermap = [];
        for(var j = 0; j < width; j++){
          innermap.push(" ");
        }
        map.push(innermap);
      }
      //console.log(map);
      this.setState({minesweeper: map, change: false});  
      //console.log(map);
    }
    //console.log(this.state.minesweeper)
    if(!this.state.change){
      for(var i = 0; i < height; i++){
        for(var j = 0; j < width; j++){
          var id = i + " " + j;
          BlockList.push(this.createBlock(id));
        }
      }
    }
    return BlockList;
  }
  renderLoadModal(){
    return(
      <div className="modal">
        <div className="modalContent">
          <h4>{this.state.difficulty} record found, Continue?</h4>
          <button onClick={()=>this.removeOldRecord()}>No</button>
          <button onClick={()=>this.setState({load: false, minesweeper: this.state.old_minesweeper, custom: false})}>Yes</button>
        </div>
      </div>
    )
  }
  customRecordUpdate(_bool){
    var payload = {
      sessionid: localStorage.getItem('sessionid'),
      win: true,
      update: _bool,
    };
    var self = this;
    axios.post(backendServerURL + '/click', payload)
    .then(function(response){
      if(response.status == 200){
        if(response.data.status == "success"){
          self.setState({customUpdate: false});
        } else {
          //error
          self.setState({win: false, error: response.data.message});
        }
      }
    });
  }
  renderCustomUpdate(){
    return(
      <>
      <h6>Custom Update</h6>
      <div>
        Old record<br/>
        Size: {this.state.oldCustomSize} Time: {this.state.oldCustomTime}<br/>
        <br/>
        New record<br/>
        Size: {this.state.height}x{this.state.width} Time: {this.state.time}<br/>
      </div>
      <button onClick={()=>this.customRecordUpdate(false)}>Keep Old record</button>
      <button onClick={()=>this.customRecordUpdate(true)}>Update new record</button>
      </>
    )
  }
  renderWinModal(){
    return(
      <div className="modal">
        <div className="modalContent">
          <h4>Win!</h4>
          <img src={firework} style={{height: '20vh'}}/><br></br>
          <button onClick={()=>this.setState({win: false})} disabled={this.state.customUpdate}>Hide pop up. </button>
          <button onClick={()=>window.location.reload(false)} disabled={this.state.customUpdate}>Restart</button>
          {this.state.customUpdate ? this.renderCustomUpdate() : null}
        </div>
      </div>
    )
  }
  customCreate() {
    var sessionid = localStorage.getItem('sessionid');
    axios.post(backendServerURL + '/create', {sessionid: sessionid, delete: true, old: true})
    .then();
    var payload = {
      sessionid: sessionid,
      difficulty: this.state.difficulty,
      height: this.state.height,
      width: this.state.width,
    };
    var self = this;
    axios.post(backendServerURL + '/create', payload)
    .then(function(response){
      self.setState({bombCount: response.data.bombNumber, change: true, time: 0, custom: false});
    });
    //this.setState({change: true, time: 0, custom: false})
  }
  renderCustomModal() {
    return(
      <div className="modal">
        <div className="modalHeader">
          <h2>Custom size</h2>
        </div>
        <div className="modalContent">
          <label>Height</label><input type="number" onChange={(event)=>event.target.value > 0 ? this.setState({height: Math.floor(event.target.value), change: true}) : this.setState({height: 1, change: true})}></input>
          <label>Width</label><input type="number" onChange={(event)=>event.target.value > 0 ? this.setState({width: Math.floor(event.target.value), change: true}) : this.setState({width: 1, change: true})}></input>
          <button onClick={()=>this.customCreate()}>OK</button>
        </div>
      </div>
    )
  }
  handleChangedifficulty(e) {
    console.log("changed");
    var sessionid = localStorage.getItem('sessionid');
    axios.post(backendServerURL + '/create', {sessionid: sessionid, delete: true});
    var payload = {
      sessionid: sessionid,
      difficulty: e.target.value,
      height: this.state.height,
      width: this.state.width,
    };
    var self = this;
    var difficulty = e.target.value;
    if(e.target.value == "custom"){
      this.setState({difficulty: e.target.value, change:true, custom: true, time: 0,});
    } else {
      axios.post(backendServerURL + '/create', payload)
      .then(function(response){
        self.setState({difficulty: difficulty, change: true, time: 0, bombCount: response.data.bombNumber});
      });
      //this.setState({difficulty: e.target.value, change: true, time: 0, });
    }
  }
  render() {
    document.addEventListener('contextmenu', function(event){event.preventDefault();})
    if(this.state.first){
      var sessionid = localStorage.getItem('sessionid');
      var self = this;
      var payload = {
        sessionid: sessionid,
        difficulty: this.state.difficulty,
      };
      axios.post(backendServerURL + '/create', payload)
      .then(function(response){
        if(response.status == 200){
          if(response.data.status == "success"){
            if(response.data.message != "create done"){
              //record exit --> continue or not? (pop up box)
              self.setState({old_minesweeper: response.data.minesweeper, difficulty: response.data.difficulty, load: true, first: false, bombCount: response.data.bombNumber, time: response.data.time, change: true}); //accept -> load to minesweeper
            } else {
              self.setState({bombCount: response.data.bombNumber});
            }
          }
        }
      });
      this.setState({first: false});
      this.tickID = setInterval(()=> this.tick(), 100);
    }
    if(this.state.boom){
      clearInterval(this.tickID);
      setTimeout(
        function(){
          this.setState({showBoomModal: true});
        }
        .bind(this),
        3000
      )
    }
    if(this.state.win){
      clearInterval(this.tickID);
      
    }
    var column = "";
    for(var i = 0; i < this.state.width; i++){
      column += "auto ";
    }
    return(
      <div className="mainContent flexBox">
        <h2>Minesweeper</h2>
        {this.state.status}
        {this.state.showBoomModal ? this.renderBoomModal() : null}
        {this.state.load ? this.renderLoadModal() : null}
        {this.state.win ? this.renderWinModal() : null}
        {this.state.difficulty == "custom" && this.state.custom && !this.state.load ? this.renderCustomModal() : null}
        <div className="gameFrame">
          <div>difficulty</div>
          <div>Bomb Count</div>
          <div>Time</div>
          <div>
            <select value={this.state.difficulty} onChange={e=>this.handleChangedifficulty(e)}>
              <option value='easy'>Easy</option>
              <option value='medium'>Medium</option>
              <option value='hard'>Hard</option>
              <option value='custom'>Custom</option>
            </select>
          </div>
          <div>{this.state.bombCount}</div>
          <div>{(this.state.time/1000).toFixed(1)}</div>
          <div className="gameContent" style={{gridTemplateColumns: column}}>{this.renderGameFrame()}</div>
        </div>
      </div>
    );
  }
}

class LeaderBoard extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      game: 'minesweeper',
      difficulty: 'easy',
      leaderboardMap: new Map(),
      first: true,
    }
  }
  renderleaderboard() {
    var leaderboardArray = [];
    for(var i = 1; i <= this.state.leaderboardMap.size; i++){
      var rankDetail = this.state.leaderboardMap.get(i);
      leaderboardArray.push(<tr><th>{rankDetail.username}</th><th>{rankDetail.time/1000}</th></tr>);
    }
    return leaderboardArray;
  }
  doPull(){
    var payload = {
      type: 'pull',
      difficulty: this.state.difficulty,
    };
    var self = this;
    axios.post(backendServerURL+"/leaderboard", payload)
    .then(function(response){
      if(response.status === 200){
        if(response.data.status === "success"){
          var leaderboardMap = new Map();
          for(var i = 0; i < response.data.message.length; i++){
            var rank = response.data.message[i].rank;
            var rankDetail = {
              username: response.data.message[i].username,
              time: response.data.message[i].time,
            }
            leaderboardMap.set(rank, rankDetail);
          }
          self.setState({
            leaderboardMap: leaderboardMap,
          });
        }
      }
    });
    this.setState({first: false});
  }
  render(){
    if(this.state.first){
      this.doPull();
    }
    return(
      <div className="mainContent flexBox">
        <select value={this.state.game} onChange={(event)=>this.setState({game: event.target.value})}>
          <option value="minesweeper">Minesweeper</option>
        </select>
        <select value={this.state.difficulty} onChange={(event)=>this.setState({difficulty: event.target.value, first: true})}>
          <option value='easy'>Easy</option>
          <option value='medium'>Medium</option>
          <option value='hard'>Hard</option>
        </select>
        <table>
          <tr>
            <th colSpan="2">{this.state.game}</th>
          </tr>
          <tr>
            <th>Username</th>
            <th>Time (s)</th>
          </tr>
          {this.state.leaderboardMap.size != 0 ? this.renderleaderboard() : null}
        </table>
      </div>
    )
  }
}

class Logout extends React.Component{
  render(){
    axios.post(backendServerURL+'/logout',{sessionid: localStorage.getItem('sessionid')});
    localStorage.removeItem('sessionid');
    setTimeout(()=>{this.props.onClick()}, 3000);
    return(
      <div className="mainContent flexBox">
        <h1>Logging out...</h1>
      </div>
    );
  }
}

class ShowBarList extends React.Component{
  renderButton(content){
    var id = content.split("/")[0].toLowerCase(); //Login (suppose unique id -> case handle)
    return(
      <button className="showBarButton" id={id} onClick={()=>this.props.onClick(id)}>{content}</button>
    )
  }
  renderButtonArray(){
    var ButtonArray = this.props.login ? ["Logout", "Profile", "Minesweeper","LeaderBoard"] : ["Login/Register", "LeaderBoard"];
    var ButtonList = [];
    ButtonArray.forEach(element => {
      ButtonList.push(<li>{this.renderButton(element)}</li>);
    });
    return ButtonList;
  }
  render() {
    return(
      <div className={this.props.show ? "showBar": "hideBar"}>
          {this.renderButtonArray()}
      </div>
    );
  }
}

class MainFrame extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      show: false,
      login: false,
      account: '',
      page: "login",
    }
  }
  mainContent() {
    if(this.state.page === "login"){
      return(
        <Login onClick={(page)=>this.showBarButtonClick(page)} onLogin={(account)=>this.login(account)}/>
      );
    } else if(this.state.page === "register"){
      return(
        <Register onClick={(page)=>this.showBarButtonClick(page)}/>
      );
    } else if(this.state.page === "minesweeper"){
      return(
        <Minesweeper/>
      );
    } else if(this.state.page === "profile"){
      return(
        <Profile/>
      );
    } else if(this.state.page === "leaderboard"){
      return(
        <LeaderBoard/>
      );
    } else if(this.state.page === "logout"){
      return(
        <Logout onClick={()=>this.logout()}/>
      );
    }
  }
  logout(){
    this.setState({page: 'login', login: false, show: false});
  }
  showBarButtonClick(page){
    this.setState({page: page, show: false});
  }
  login(account){
    this.setState({account: account, login: true});
  }
  render() {
    if(localStorage.getItem('sessionid') && this.state.login == false){
      this.setState({login: true, page: "minesweeper"});
    }
    return(
      <div>
        <nav>
          <img src={show} alt='show' onClick={()=>this.setState({show: !this.state.show})}></img>
        </nav>
        <section>
          <ShowBarList show={this.state.show} login={this.state.login} onClick={(page)=>this.showBarButtonClick(page)}/>
        </section>
        <div className="flexBox">
          {this.mainContent()}
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <MainFrame/>, document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
